package com.fitzgerald_gmbh.e_portfolio_kevin_kussmaul_musicplayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing MediaPlayer.
 *
 * @author Kevin Kußmaul
 * @version 1.0
 *
 * Created on 2016-06-01 07:46:34 AM
 */
public class MediaPlayer {

     private List<Media> playlist = new ArrayList<>();
    private int pos = 0;

    public MediaPlayer(Media media) {
        playlist.add(media);
    }

    public MediaPlayer() {
    }

    public void playOrPause() {
        if (gotSongsAndNotDonePlaying()) {
            switch (getPlaylist().get(pos).getState()) {
                case PLAYING:
                    getPlaylist().get(pos).pause();
                    break;
                case PAUSED:
                    getPlaylist().get(pos).play();
                    break;
                default:
                    break;
            }
        } else if (pos == getPlaylist().size() - 1) {
            switch (getPlaylist().get(pos).getState()) {
                case PLAYING:
                    getPlaylist().get(pos).pause();
                    break;
                case PAUSED:
                    getPlaylist().get(pos).play();
                    break;
                default:
                    break;
            }
        }
    }

    public boolean stop() {
        if (gotSongsAndNotDonePlaying() && getPlaylist().get(pos).getState() != MediaState.ERROR) {
            getPlaylist().get(pos).stop();
        }
        return getPlaylist().get(pos).getState() == MediaState.STOPPED;
    }

    public boolean skip() {
        if (!getPlaylist().isEmpty() && pos < getPlaylist().size() - 1) {

            getPlaylist().get(pos).stop();
            pos++;
            return true;
        }
        return false;
    }

    public boolean gotSongsAndNotDonePlaying() {
        return !getPlaylist().isEmpty() && pos < getPlaylist().size();
    }

    public double calcProgress() {
        if (gotSongsAndNotDonePlaying()) {
            final Media media = getPlaylist().get(pos);
            final double quot = (double) media.getCurrentPos() / media.getDuration();
            final double progress = quot * 100.0;
            // round on 2 digits
            return ((double) ((int) (progress * 100))) / 100;
        }
        return 0;
    }

    /**
     * @return the playlist
     */
    public List<Media> getPlaylist() {
        return playlist;
    }

}
