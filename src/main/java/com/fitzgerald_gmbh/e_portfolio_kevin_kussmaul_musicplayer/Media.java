package com.fitzgerald_gmbh.e_portfolio_kevin_kussmaul_musicplayer;

import java.util.HashMap;
import java.util.Map;

/**
 * Class representing Media.
 *
 * @author Kevin Kußmaul
 * @version 1.0
 *
 * Created on 2016-06-01 07:36:10 AM
 */
public class Media {
    private MediaState state = MediaState.NEW;
    private Map<String, Object> metadata = new HashMap<>(3);
    private String url;
    private long duration = 0L;

    public Media(String url) {
        this.url = url;
    }
    
    public boolean play() {
          //NW
        return false;
    }
    
    public boolean pause() {
          //NW
        return false;
    }
    
    public boolean stop() {
        //NW
        return false;
    }
    

    public MediaState getState() {
        return state;
    }

    public String getUrl() {
        return url;
    }

    public long getDuration() {
        return duration;
    }
    
     public long getCurrentPos() {
        return duration;
    }
    
    
    public Map<String, Object> getMetadata() {
        return metadata;
    }

    
}