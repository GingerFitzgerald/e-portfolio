package com.fitzgerald_gmbh.e_portfolio_kevin_kussmaul_musicplayer;

/**
 * Enumeration of MediaStates.
 *
 * @author Kevin Kußmaul
 * @version 1.0
 *
 * Created on 2016-06-01 07:42:50 AM
 */
public enum MediaState {

    NEW,
    READY,
    PLAYING,
    PAUSED,
    STOPPED,
    ERROR;

}
