package com.fitzgerald_gmbh.e_portfolio_kevin_kussmaul_musicplayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Kevin Kußmaul
 */
public class MediaPlayerTest {

    public MediaPlayerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of skip method, of class MediaPlayer.
     */
    @Test
    public void testSkip() {
        System.out.println("skip");
        // mock metadata
        final HashMap<String, Object> firstMetadata = new HashMap<>(2);
        firstMetadata.put("artist", "First Artist");
        firstMetadata.put("title", "First Title");
        firstMetadata.put("album", "First Album");
        final HashMap<String, Object> secondMetadata = new HashMap<>(2);
        secondMetadata.put("artist", "Second Artist");
        secondMetadata.put("title", "Second Title");
        secondMetadata.put("album", "Second Album");
        // mock media
        Media first = mock(Media.class);
        Media second = mock(Media.class);
        // make mocked metadata be returned
        when(first.getMetadata()).thenReturn(firstMetadata);
        when(second.getMetadata()).thenReturn(secondMetadata);
        // make stop call real method
        when(first.stop()).thenCallRealMethod();
        when(second.stop()).thenCallRealMethod();
        //mock status to playing
        MediaState status = MediaState.PLAYING;
        when(first.getState()).thenReturn(status);
        when(second.getState()).thenReturn(status);

        // mock media player
        MediaPlayer mockedMediaPlayer = mock(MediaPlayer.class);
        when(mockedMediaPlayer.skip()).thenCallRealMethod();
        // mock playlist
        List<Media> playlist = new ArrayList<>(2);
        playlist.add(0, first);
        playlist.add(1, second);
        // make mediaPlayer use the mocked playlist
        when(mockedMediaPlayer.getPlaylist()).thenReturn(playlist);
        // perform skip
        mockedMediaPlayer.skip();
        // verify stop has been invoked
        verify(first, times(1)).stop();

    }

    @Test
    public void testCalcProgress() {
        System.out.println("calcProgress");
        long durataion = 120l;
        long current = 60l;
        double expectedResult = 50d;
        Media media = mock(Media.class);

        // mock media to return given duration and cureent time
        when(media.getDuration()).thenReturn(durataion);
        when(media.getCurrentPos()).thenReturn(current);
        // mock mediaplayer
        MediaPlayer player = mock(MediaPlayer.class);
        when(player.getPlaylist()).thenReturn(Arrays.asList(media));
        when(player.calcProgress()).thenCallRealMethod();
        when(player.gotSongsAndNotDonePlaying()).thenReturn(true);
        // call calcProgress
        double result = player.calcProgress();
        // check result
        assert result == expectedResult;
    }

    /**
     * Test of stop method, of class MediaPlayer.
     */
    @Test
    public void testStop() {
        System.out.println("stop");
        // mock media
        Media media = mock(Media.class);
        // mock playlist
        List<Media> playlist = new ArrayList<>();
        playlist.add(0, media);
        // mock media state
        MediaState state = MediaState.PLAYING;
        when(media.getState()).thenReturn(state);
        when(media.stop()).thenCallRealMethod();
        // mock player
        MediaPlayer player = mock(MediaPlayer.class);
        when(player.getPlaylist()).thenReturn(playlist);
        when(player.gotSongsAndNotDonePlaying()).thenReturn(true);
        when(player.stop()).thenCallRealMethod();
        // call stop
        player.stop();
        verify(media, Mockito.atLeastOnce()).stop();
        // call stop on mocked MediaPlayer, verify stop is called on Media
    }

}
